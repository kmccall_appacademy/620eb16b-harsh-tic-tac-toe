
require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_reader :board, :current_player, :players

  def initialize(player_one,player_two)
    @players = [player_one,player_two]
    @board = Board.new
    @players.first.mark = :X
    @players.last.mark = :Y
    @current_player = @players.first
  end

  def play_turn
    flag = false
    until flag
      @current_player.display(@board)
      pos = @current_player.get_move
      mark = @current_player.mark
      @board.place_mark(pos,mark)
      flag = true
    end
    switch_players!
  end

  def switch_players!
    @players.reverse!
    @current_player = @players.first
  end

end
