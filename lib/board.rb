class Board
  attr_accessor :grid

  attr_reader :winner_groups

  def initialize(grid=[[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]])
    @grid = grid
    set_winner_groups
  end

  def set_winner_groups
    @winner_groups = []
    set_winner_rows_cols("row")
    set_winner_rows_cols("col")
    set_winner_diag("left")
    set_winner_diag("right")
  end

  def set_winner_rows_cols(str)
    length = @grid.length
    (0...length).each do |idx1|
      positions = []
      (0...length).each do |idx2|
        case str
        when "row"
          positions << [idx1,idx2]
        when "col"
          positions << [idx2,idx1]
        end
      end
      @winner_groups << positions
    end
  end

  def set_winner_diag(str)
    length = @grid.length
    positions = []
    (0...length).each do |idx|
      case str
        when "left"
          positions << [idx,idx]
        when "right"
          positions << [idx,length-1-idx]
      end
    end
    @winner_groups << positions
  end

  def place_mark(pos,mark)
    self[pos]=mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    @winner_groups.any? do |group|
      value = self[group.first]
      return value if group.all? {|pos| self[pos] == value} && !value.nil?
    end
    nil
  end

  def over?
    return true unless winner.nil?
    @grid.flatten.none? {|value| value.nil?}
  end

  def [](pos)
   row, col = pos
   @grid[row][col]
 end

 def []=(pos, mark)
   row, col = pos
   @grid[row][col] = mark
 end

end
