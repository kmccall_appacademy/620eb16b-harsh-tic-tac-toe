class ComputerPlayer

  attr_accessor :mark

  attr_reader :name, :board

  def initialize(name)
    @name = name
  end

  def get_move
    winner_groups=@board.winner_groups.dup
    flag = false
    move = [rand(@board.grid.length),rand(@board.grid.length)]
    possible_groups = winner_groups.select do |group|
      group.count {|pos| @board[pos]==mark} == 2 && group.any? {|pos| @board[pos].nil?}
    end
    move = possible_groups.first.select {|pos| @board[pos].nil?}.flatten unless possible_groups.empty?()
    move
  end

  def display(board)
    @board = board
    @board.grid.each do |row|
      row.each do |value|
        print "#{render_value(value)} "
      end
      print "\n\n"
    end
  end

  def render_value(value)
    case value
      when :X
        return "X"
      when :Y
        return "Y"
      when nil
        return "_"
    end
  end



end
