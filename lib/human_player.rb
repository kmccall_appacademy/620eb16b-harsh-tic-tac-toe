class HumanPlayer

  attr_accessor :mark

  attr_reader :name, :board

  def initialize(name)
    @name = name
  end

  def get_move
    flag = false
    print "Enter where to mark: (X, Y)"
    pos_string = gets.chomp
    pos = pos_string.delete(',').split.map(&:to_i)
    pos
  end

  def display(board)
    @board = board
    board.grid.each do |row|
      row.each do |value|
        print "#{render_value(value)} "
      end
      print "\n\n"
    end
  end

  def render_value(value)
    case value
      when :X
        return "X"
      when :Y
        return "Y"
      when nil
        return "_"
    end
  end

end
